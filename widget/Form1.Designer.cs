﻿namespace widget
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.timer_processes = new System.Windows.Forms.Timer(this.components);
            this.mediaPlayer = new AxWMPLib.AxWindowsMediaPlayer();
            this.tm_media = new System.Windows.Forms.Timer(this.components);
            this.ptb_weather = new System.Windows.Forms.PictureBox();
            this.ptb_media = new System.Windows.Forms.PictureBox();
            this.ptb_mini = new System.Windows.Forms.PictureBox();
            this.ptb_calendar = new System.Windows.Forms.PictureBox();
            this.ptb_clock = new System.Windows.Forms.PictureBox();
            this.ptb_cpu = new System.Windows.Forms.PictureBox();
            this.ptb_exit = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.mediaPlayer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptb_weather)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptb_media)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptb_mini)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptb_calendar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptb_clock)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptb_cpu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptb_exit)).BeginInit();
            this.SuspendLayout();
            // 
            // timer_processes
            // 
            this.timer_processes.Interval = 1000;
            this.timer_processes.Tick += new System.EventHandler(this.timer_processes_Tick);
            // 
            // mediaPlayer
            // 
            this.mediaPlayer.Enabled = true;
            this.mediaPlayer.Location = new System.Drawing.Point(10, 34);
            this.mediaPlayer.Name = "mediaPlayer";
            this.mediaPlayer.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("mediaPlayer.OcxState")));
            this.mediaPlayer.Size = new System.Drawing.Size(160, 45);
            this.mediaPlayer.TabIndex = 5;
            this.mediaPlayer.Visible = false;
            // 
            // tm_media
            // 
            this.tm_media.Interval = 1000;
            this.tm_media.Tick += new System.EventHandler(this.tm_media_Tick);
            // 
            // ptb_weather
            // 
            this.ptb_weather.Image = global::widget.Properties.Resources.bg_weather;
            this.ptb_weather.Location = new System.Drawing.Point(10, 391);
            this.ptb_weather.Name = "ptb_weather";
            this.ptb_weather.Size = new System.Drawing.Size(358, 170);
            this.ptb_weather.TabIndex = 7;
            this.ptb_weather.TabStop = false;
            // 
            // ptb_media
            // 
            this.ptb_media.Location = new System.Drawing.Point(10, 580);
            this.ptb_media.Name = "ptb_media";
            this.ptb_media.Size = new System.Drawing.Size(358, 300);
            this.ptb_media.TabIndex = 6;
            this.ptb_media.TabStop = false;
            this.ptb_media.Click += new System.EventHandler(this.ptb_media_Click);
            this.ptb_media.DoubleClick += new System.EventHandler(this.ptb_media_DoubleClick);
            this.ptb_media.MouseMove += new System.Windows.Forms.MouseEventHandler(this.ptb_media_MouseMove);
            // 
            // ptb_mini
            // 
            this.ptb_mini.Image = global::widget.Properties.Resources.mini1;
            this.ptb_mini.Location = new System.Drawing.Point(10, 85);
            this.ptb_mini.Name = "ptb_mini";
            this.ptb_mini.Size = new System.Drawing.Size(160, 100);
            this.ptb_mini.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.ptb_mini.TabIndex = 4;
            this.ptb_mini.TabStop = false;
            // 
            // ptb_calendar
            // 
            this.ptb_calendar.Location = new System.Drawing.Point(176, 210);
            this.ptb_calendar.Name = "ptb_calendar";
            this.ptb_calendar.Size = new System.Drawing.Size(202, 165);
            this.ptb_calendar.TabIndex = 3;
            this.ptb_calendar.TabStop = false;
            // 
            // ptb_clock
            // 
            this.ptb_clock.Location = new System.Drawing.Point(10, 215);
            this.ptb_clock.Name = "ptb_clock";
            this.ptb_clock.Size = new System.Drawing.Size(160, 160);
            this.ptb_clock.TabIndex = 2;
            this.ptb_clock.TabStop = false;
            // 
            // ptb_cpu
            // 
            this.ptb_cpu.BackColor = System.Drawing.Color.Transparent;
            this.ptb_cpu.Location = new System.Drawing.Point(178, 55);
            this.ptb_cpu.Name = "ptb_cpu";
            this.ptb_cpu.Size = new System.Drawing.Size(190, 130);
            this.ptb_cpu.TabIndex = 1;
            this.ptb_cpu.TabStop = false;
            // 
            // ptb_exit
            // 
            this.ptb_exit.Location = new System.Drawing.Point(353, 12);
            this.ptb_exit.Name = "ptb_exit";
            this.ptb_exit.Size = new System.Drawing.Size(15, 15);
            this.ptb_exit.TabIndex = 0;
            this.ptb_exit.TabStop = false;
            this.ptb_exit.Click += new System.EventHandler(this.ptb_exit_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(52)))));
            this.ClientSize = new System.Drawing.Size(380, 1000);
            this.Controls.Add(this.ptb_weather);
            this.Controls.Add(this.ptb_media);
            this.Controls.Add(this.mediaPlayer);
            this.Controls.Add(this.ptb_mini);
            this.Controls.Add(this.ptb_calendar);
            this.Controls.Add(this.ptb_clock);
            this.Controls.Add(this.ptb_cpu);
            this.Controls.Add(this.ptb_exit);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Form1";
            this.ShowInTaskbar = false;
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Form1_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.mediaPlayer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptb_weather)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptb_media)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptb_mini)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptb_calendar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptb_clock)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptb_cpu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptb_exit)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox ptb_exit;
        private System.Windows.Forms.Timer timer_processes;
        public System.Windows.Forms.PictureBox ptb_media;
        public AxWMPLib.AxWindowsMediaPlayer mediaPlayer;
        public System.Windows.Forms.Timer tm_media;
        public System.Windows.Forms.PictureBox ptb_mini;
        public System.Windows.Forms.PictureBox ptb_cpu;
        public System.Windows.Forms.PictureBox ptb_clock;
        public System.Windows.Forms.PictureBox ptb_calendar;
        public System.Windows.Forms.PictureBox ptb_weather;
    }
}

