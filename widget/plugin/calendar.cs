﻿using System;
using System.Drawing;

namespace widget
{
    public class calendar
    {
        string[] _dayOfWeek = new string[] { "Su", "Mo", "Tu", "We", "Th", "Fr", "Sa" };
        string[] _month = new string[] { "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" };
        DateTime date = DateTime.Now;
        Font _font = new Font("Arial", 8);
        Font _yearF = new Font("Arial", 10);
        SolidBrush _brush = new SolidBrush(Color.White);
        Pen _penBlack = new Pen(Color.Black, 1);
        SolidBrush _brushDay = new SolidBrush(Color.FromArgb(0, 183, 195));

        public Bitmap drawCalendar()
        {
            Form1 frm1 = new Form1();
            double _space = (frm1.ptb_calendar.Width - 10) / 7;
            Bitmap bm = new Bitmap(frm1.ptb_calendar.Width, frm1.ptb_calendar.Height);
            Graphics g = Graphics.FromImage(bm);
            // g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;

            g.DrawString(_month[date.Month - 1] + ", " + date.Year.ToString(), _yearF, _brush, 0, 0);
            for (int i = 0; i < 7; i++)
            {
                g.DrawString(_dayOfWeek[i], _font, _brush, (float)(i * _space) + 5, 28);
            }
            int _daysInMonth = DateTime.DaysInMonth(date.Year, date.Month);
            int _day1OfWeek = (int)(new DateTime(date.Year, date.Month, 1)).DayOfWeek;
            for (int i = 0; i < _daysInMonth; i++)
            {
                g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
                DateTime dateTmp = new DateTime(date.Year, date.Month, i + 1);
                float x = (float)((int)(dateTmp.DayOfWeek) * _space) + 5;
                float y = (((i + _day1OfWeek) / 7) * (float)(_space - 3)) + 50;

                if (i + 1 == date.Day)
                {
                    g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.Default;
                    g.FillRectangle(_brushDay, (i + 1) < 10 ? x - 3 : x - 4, y - 2, 24, 18);
                    g.DrawRectangle(_penBlack, (i + 1) < 10 ? x - 1 : x - 2, y - 1, 19, 15);
                }
                g.DrawString((i + 1).ToString(), _font, _brush, (i + 1) < 10 ? x + 4 : x, y);
            }
            GC.Collect();
            return bm;
        }
    }
}
