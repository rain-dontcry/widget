﻿using System;
using System.Drawing;

namespace widget
{
    public class exit
    {
        Pen _penCls = new Pen(Color.FromArgb(0, 183, 195), 1);

        public Bitmap drawExitButton()
        {
            Bitmap bm = new Bitmap(15, 15);
            Graphics g = Graphics.FromImage(bm);
            g.DrawLine(_penCls, 1, 0, 14, 13);
            g.DrawLine(_penCls, 0, 1, 13, 14);
            g.DrawLine(_penCls, 0, 13, 13, 0);
            g.DrawLine(_penCls, 1, 14, 14, 1);
            GC.Collect();
            return bm;
        }
    }
}
