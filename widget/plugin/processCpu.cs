﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Management;

namespace widget
{
    public class processCpu
    {
        Font _font = new Font("Arial", 8);
        Pen _pen = new Pen(Color.FromArgb(250, 190, 110), 1);
        SolidBrush _brush = new SolidBrush(Color.White);
        readonly SolidBrush _brushTran = new SolidBrush(Color.FromArgb(30, 250, 190, 110));

        //-- Processes
        PerformanceCounter cpuCounter;
        float[] _cpuArr = new float[41];
        object _cpuName = "";

        public string systemInfo()
        {
            SelectQuery query = new SelectQuery(@"Select * from Win32_Processor");
            using (ManagementObjectSearcher searcher = new ManagementObjectSearcher(query))
            {
                foreach (ManagementObject process in searcher.Get())
                {
                    _cpuName = process["Name"];
                }
            }

            //string[] _strTmp = _cpuName.ToString().Split(new string[] { " CPU" }, StringSplitOptions.None);
            //_cpuName = _strTmp[0] + Environment.NewLine + "CPU" + _strTmp[1];
            return _cpuName.ToString();
        }

        public Bitmap drawProcesses(string cpuName, Form1 frm)
        {
            int wid = frm.ptb_cpu.Width;
            int hei = frm.ptb_cpu.Height;
            cpuCounter = new PerformanceCounter("Processor", "% Processor Time", "_Total");
            cpuCounter.NextValue();
            System.Threading.Thread.Sleep(300);
            int _cpu = (int)cpuCounter.NextValue();
            for (int i = 0; i < 40; i++)
            {
                _cpuArr[i] = _cpuArr[i + 1];
            }
            _cpuArr[40] = _cpu;
            //-- Draw processes
            Bitmap bm = new Bitmap(wid, hei);
            Graphics g2 = Graphics.FromImage(bm);
            g2.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
            g2.DrawLine(_pen, 0, 30, 0, hei - 1);
            g2.DrawLine(_pen, 0, hei - 1, wid, hei - 1);
            g2.DrawString("100%", _font, _brush, wid - 30, 12);
            g2.DrawString(cpuName.ToString(), _font, _brush, -2, 0);
            for (int i = 0; i < 40; i++)
            {
                // Delta x
                float nodeWid = (float)(wid / 40.0);
                double x1 = i * nodeWid;
                double x2 = (i + 1) * nodeWid;
                // Draw CPU
                g2.DrawLine(_pen, (float)x1, hei - _cpuArr[i], (float)x2, hei - _cpuArr[i + 1]);
                Point[] _ptnTmp = new Point[] { new Point((int)x1, (int)(hei - _cpuArr[i])), new Point((int)x2, (int)(hei - _cpuArr[i + 1])), new Point((int)x2, hei - 1), new Point((int)x1, hei - 1) };
                g2.FillPolygon(_brushTran, _ptnTmp);
            }
            GC.Collect();
            return bm;
        }
    }
}
