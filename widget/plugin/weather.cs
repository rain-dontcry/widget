﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace widget.plugin
{
    public class weather
    {
        // http://home.openweathermap.org/users/sign_in -- link đăng ký lấy API
        private const string API_KEY = "e421312a9cc938dd3364ec76d9727789";
        private const string idCity = "1566083";
        private const string weatherUrl = "http://api.openweathermap.org/data/2.5/weather?id=" + idCity + "&mode=xml&units=imperial&APPID=" + API_KEY;

        // graphics
        Font f8 = new Font("Tahoma", 8);
        Font f9 = new Font("Tahoma", 9);
        Font f12 = new Font("Tahoma", 12);
        Font f16 = new Font("Tahoma", 16);
        Font f32 = new Font("Tahoma", 32);
        Pen _p = new Pen(Color.White, 1);
        Pen _fillP = new Pen(Color.FromArgb(70, 66, 72, 128), 1);
        SolidBrush _brush = new SolidBrush(Color.White);
        SolidBrush _fillBrus = new SolidBrush(Color.FromArgb(60, 66, 72, 128));
        Rectangle _rec;
        Bitmap bm;

        public Image DownloadImageFromUrl(string imageUrl)
        {
            Image image;

            try
            {
                HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(imageUrl);
                webRequest.AllowWriteStreamBuffering = true;
                webRequest.Timeout = 30000;

                WebResponse webResponse = webRequest.GetResponse();

                Stream stream = webResponse.GetResponseStream();

                image = System.Drawing.Image.FromStream(stream);

                webResponse.Close();
                return image;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static GraphicsPath RoundedRect(Rectangle bounds, int radius)
        {
            int diameter = radius * 2;
            Size size = new Size(diameter, diameter);
            Rectangle arc = new Rectangle(bounds.Location, size);
            GraphicsPath path = new GraphicsPath();

            if (radius == 0)
            {
                path.AddRectangle(bounds);
                return path;
            }

            // top left arc  
            path.AddArc(arc, 180, 90);

            // top right arc  
            arc.X = bounds.Right - diameter;
            path.AddArc(arc, 270, 90);

            // bottom right arc  
            arc.Y = bounds.Bottom - diameter;
            path.AddArc(arc, 0, 90);

            // bottom left arc 
            arc.X = bounds.Left;
            path.AddArc(arc, 90, 90);

            path.CloseFigure();
            return path;
        }

        public Bitmap getDataWeather(Form1 frm)
        {
            bm = new Bitmap(frm.ptb_weather.Image);
            using (WebClient client = new WebClient())
            {
                try
                {
                    string xml = client.DownloadString(weatherUrl);
                    XmlDocument xml_doc = new XmlDocument();
                    xml_doc.LoadXml(xml);

                    Image sunriseIco = Properties.Resources.sunrise;
                    Image sunsetIco = Properties.Resources.sunset;
                    Image humidityIco = Properties.Resources.humidity;
                    Image windIco = Properties.Resources.wind;
                    Image pressureIco = Properties.Resources.pressure;
                    Image precipitationIco = Properties.Resources.precipitation;

                    // get data weather
                    XmlNode loc_node = xml_doc.SelectSingleNode("current");
                    string temperature = loc_node.SelectSingleNode("temperature").Attributes["value"].Value;
                    string minTemperat = loc_node.SelectSingleNode("temperature").Attributes["min"].Value;
                    string maxTemperat = loc_node.SelectSingleNode("temperature").Attributes["max"].Value;
                    string clouds = loc_node.SelectSingleNode("clouds").Attributes["name"].Value;
                    string humidity = loc_node.SelectSingleNode("humidity").Attributes["value"].Value;
                    string iconCode = loc_node.SelectSingleNode("weather").Attributes["icon"].Value;
                    string wind = loc_node.SelectSingleNode("wind/speed").Attributes["value"].Value;
                    string pressure = loc_node.SelectSingleNode("pressure").Attributes["value"].Value;
                    string sunrise = loc_node.SelectSingleNode("city/sun").Attributes["rise"].Value;
                    string sunset = loc_node.SelectSingleNode("city/sun").Attributes["set"].Value;
                    string precipitation = loc_node.SelectSingleNode("precipitation").Attributes["mode"].Value;
                    
                    // convert data string
                    Graphics g = Graphics.FromImage(bm);
                    g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
                    float toC = (float.Parse(temperature, CultureInfo.InvariantCulture) - 32) * 5 / 9;
                    float toCMin = (float.Parse(minTemperat, CultureInfo.InvariantCulture) - 32) * 5 / 9;
                    float toCMax = (float.Parse(maxTemperat, CultureInfo.InvariantCulture) - 32) * 5 / 9;
                    DateTime _sunrise = Convert.ToDateTime(sunrise);
                    DateTime _sunset = Convert.ToDateTime(sunset);
                    if(precipitation == "no")
                    {
                        precipitation = "N/A";
                    }

                    // Draw to weather panel
                    g.DrawString("- Sài Gòn -", f12, _brush, 10, 10);
                    Image img = DownloadImageFromUrl("http://openweathermap.org/img/w/" + iconCode + ".png");
                    g.DrawImage(img, 75, 30, 50, 50);
                    g.DrawString(clouds, f9, _brush, 25, 80);
                    g.DrawString(Math.Round(toC, 1).ToString(), f32, _brush, 5, 105);
                    g.DrawString("°", f16, _brush, 56, 112);
                    g.DrawString(Math.Round(toCMin).ToString() + "°C", f8, _brush, 89, 122);
                    g.DrawLine(_p, 89, 136, 116, 136);
                    g.DrawString(Math.Round(toCMax).ToString() + "°C", f8, _brush, 89, 137);

                    g.DrawLine(_p, 130, 20, 130, 150);

                    // Humidity
                    _rec = new Rectangle(140, 20, 60, 60);
                    g.DrawPath(_fillP, RoundedRect(_rec, 4));
                    g.FillPath(_fillBrus, RoundedRect(_rec, 4));
                    g.DrawImage(humidityIco, 162, 27, 19, 19);
                    g.DrawString("Humidity", f8, _brush, 146, 50);
                    g.DrawString(humidity + " %", f8, _brush, 158, 64);

                    // Wind
                    _rec = new Rectangle(210, 20, 60, 60);
                    g.DrawPath(_fillP, RoundedRect(_rec, 4));
                    g.FillPath(_fillBrus, RoundedRect(_rec, 4));
                    g.DrawImage(windIco, 229, 25, 22, 20);
                    g.DrawString("Wind", f8, _brush, 226, 50);
                    g.DrawString(wind + " mph", f8, _brush, 212, 64);

                    // Pressure
                    _rec = new Rectangle(280, 20, 60, 60);
                    g.DrawPath(_fillP, RoundedRect(_rec, 4));
                    g.FillPath(_fillBrus, RoundedRect(_rec, 4));
                    g.DrawImage(pressureIco, 301, 26, 21, 22);
                    g.DrawString("Pressure", f8, _brush, 288, 50);
                    g.DrawString((Convert.ToInt32(pressure) * 0.03).ToString().PadRight(5, '0') + " in", f8, _brush, 290, 64);

                    // Sunrise
                    _rec = new Rectangle(140, 90, 60, 60);
                    g.DrawPath(_fillP, RoundedRect(_rec, 4));
                    g.FillPath(_fillBrus, RoundedRect(_rec, 4));
                    g.DrawImage(sunriseIco, 162, 97, 19, 19);
                    g.DrawString("Sunrise", f8, _brush, 151, 120);
                    g.DrawString((_sunrise.Hour - 17).ToString().PadLeft(2, '0') + ":" + (_sunrise.Minute).ToString().PadLeft(2, '0'), f8, _brush, 155, 134);

                    // Sunset
                    _rec = new Rectangle(210, 90, 60, 60);
                    g.DrawPath(_fillP, RoundedRect(_rec, 4));
                    g.FillPath(_fillBrus, RoundedRect(_rec, 4));
                    g.DrawImage(sunsetIco, 232, 97, 20, 19);
                    g.DrawString("Sunset", f8, _brush, 222, 120);
                    g.DrawString((_sunset.Hour + 7).ToString().PadLeft(2, '0') + ":" + (_sunset.Minute).ToString().PadLeft(2, '0'), f8, _brush, 225, 134);

                    // Precipitation
                    _rec = new Rectangle(280, 90, 60, 60);
                    g.DrawPath(_fillP, RoundedRect(_rec, 4));
                    g.FillPath(_fillBrus, RoundedRect(_rec, 4));
                    g.DrawImage(precipitationIco, 302, 97, 19, 18);
                    g.DrawString("Precipitation", f8, _brush, 277, 120);
                    g.DrawString(precipitation, f8, _brush, 300, 134);

                    return bm;
                }
                catch (WebException ex)
                {
                    DisplayError(ex);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Unknown error\n" + ex.Message);
                }
            }
            return bm;
        }

        private void DisplayError(WebException exception)
        {
            try
            {
                StreamReader reader = new StreamReader(exception.Response.GetResponseStream());
            }
            catch (Exception ex)
            {
                MessageBox.Show("Web error\n" + ex.Message);
            }
        }
    }
}
