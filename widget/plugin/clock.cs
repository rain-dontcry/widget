﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace widget
{
    public class clock
    {
        Pen _pen = new Pen(Color.White, 1);
        Font _font = new Font("Arial", 10);
        SolidBrush _brush = new SolidBrush(Color.White);

        // position of second, minute and hour
        readonly Point[] _second = new Point[] { new Point(80, 25), new Point(80, 93) };
        Point[] _minute = new Point[] { new Point(80, 34), new Point(82, 71), new Point(78, 71) };
        Point[] _hour = new Point[] { new Point(80, 43), new Point(84, 73), new Point(76, 73) };

        private float spinX(double defre, float x, float y, float x0, float y0)
        {
            double xTmp = (x - x0) * Math.Cos(defre) - (y - y0) * Math.Sin(defre) + x0;
            return (float)xTmp;
        }

        private float spinY(double defre, float x, float y, float x0, float y0)
        {
            double yTmp = (x - x0) * Math.Sin(defre) + (y - y0) * Math.Cos(defre) + y0;
            return (float)yTmp;
        }

        private float[,] secSpin(double defre, int w, int h)
        {
            float[,] pnts = new float[2, 2];
            for (int i = 0; i < 2; i++)
            {
                float _x = spinX(defre, _second[i].X, _second[i].Y, w / 2, h / 2);
                float _y = spinY(defre, _second[i].X, _second[i].Y, w / 2, h / 2);
                pnts[i, 0] = _x;
                pnts[i, 1] = _y;
            }
            return pnts;
        }

        private float[,] GetPoints(double defre, Point[] _ptn, int w, int h)
        {
            float[,] _multiPtn = new float[3, 2];
            for (int i = 0; i < 3; i++)
            {
                float _x = spinX(defre, _ptn[i].X, _ptn[i].Y, w / 2, h / 2);
                float _y = spinY(defre, _ptn[i].X, _ptn[i].Y, w / 2, h / 2);
                _multiPtn[i, 0] = _x;
                _multiPtn[i, 1] = _y;
            }
            return _multiPtn;
        }

        public Bitmap drawClock(Form1 frm)
        {
            int wid = frm.ptb_clock.Width;
            int hei = frm.ptb_clock.Height;
            Bitmap bmClock = new Bitmap(wid, hei);
            Graphics g = Graphics.FromImage(bmClock);
            g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
            float x = (wid / 2) - 5;
            float y = 6;
            double defre = Math.PI * 30 / 180;
            // Draw clock
            for (int i = 1; i < 13; i++)
            {
                double xTmp = spinX(defre, x, y, (wid / 2) - 5, (hei / 2) - 7);
                double yTmp = spinY(defre, x, y, (wid / 2) - 5, (hei / 2) - 7);
                x = (float)xTmp;
                y = (float)yTmp;
                x = i > 9 ? x - 4 : x;
                g.DrawString(i.ToString(), _font, _brush, x, y);
                x = (float)xTmp;
            }
            g.DrawEllipse(_pen, 0, 0, wid - 1, hei - 1);
            float pointRotateX = wid / 2;
            float pointRotateY = 25;
            double defre6 = Math.PI * 6 / 180;
            for (int i = 0; i < 360; i += 6)
            {
                float xTmp = spinX(defre6, pointRotateX, pointRotateY, wid / 2, hei / 2);
                float yTmp = spinY(defre6, pointRotateX, pointRotateY, wid / 2, hei / 2);
                pointRotateX = xTmp;
                pointRotateY = yTmp;
                switch (i)
                {
                    case 84:
                        {
                            g.FillRectangle(_brush, (pointRotateX - 10), pointRotateY, 10, 1);
                            break;
                        }
                    case 174:
                        {
                            g.FillRectangle(_brush, pointRotateX, (pointRotateY - 10), 1, 10);
                            break;
                        }
                    case 264:
                        {
                            g.FillRectangle(_brush, pointRotateX, pointRotateY, 10, 1);
                            break;
                        }
                    case 354:
                        {
                            g.FillRectangle(_brush, pointRotateX, pointRotateY, 1, 10);
                            break;
                        }
                    default:
                        {
                            g.DrawEllipse(_pen, pointRotateX, pointRotateY, 1, 1);
                            break;
                        }
                }
            }
            GC.Collect();
            return bmClock;
        }

        public Bitmap runClock(Bitmap bmC, Form1 frm)
        {
            int wid = frm.ptb_clock.Width;
            int hei = frm.ptb_clock.Height;
            // Setting before draw a clock
            Bitmap bm = new Bitmap(bmC);
            Graphics g = Graphics.FromImage(bm);
            g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;

            DateTime date = DateTime.Now;
            double _secondRotate = Math.PI * (6 * date.Second) / 180;
            double _minuteRotate = Math.PI * (6 * date.Minute) / 180 + ((_secondRotate * 6) / 360);
            double _hourRotate = Math.PI * (30 * date.Hour) / 180 + ((_minuteRotate * 30) / 360);

            // Draw second
            float[,] _secPtn = secSpin(_secondRotate, wid, hei);
            g.DrawLine(_pen, _secPtn[0, 0], _secPtn[0, 1], _secPtn[1, 0], _secPtn[1, 1]);

            // Draw minute
            float[,] _minPtn = GetPoints(_minuteRotate, _minute, wid, hei);
            g.DrawLine(_pen, _minPtn[0, 0], _minPtn[0, 1], _minPtn[1, 0], _minPtn[1, 1]);
            g.DrawLine(_pen, _minPtn[1, 0], _minPtn[1, 1], wid / 2, hei / 2);
            g.DrawLine(_pen, wid / 2, hei / 2, _minPtn[2, 0], _minPtn[2, 1]);
            g.DrawLine(_pen, _minPtn[2, 0], _minPtn[2, 1], _minPtn[0, 0], _minPtn[0, 1]);

            // Draw minute
            float[,] _houPtn = GetPoints(_hourRotate, _hour, wid, hei);
            g.DrawLine(_pen, _houPtn[0, 0], _houPtn[0, 1], _houPtn[1, 0], _houPtn[1, 1]);
            g.DrawLine(_pen, _houPtn[1, 0], _houPtn[1, 1], wid / 2, hei / 2);
            g.DrawLine(_pen, wid / 2, hei / 2, _houPtn[2, 0], _houPtn[2, 1]);
            g.DrawLine(_pen, _houPtn[2, 0], _houPtn[2, 1], _houPtn[0, 0], _houPtn[0, 1]);

            GC.Collect();
            return bm;
        }
    }
}
