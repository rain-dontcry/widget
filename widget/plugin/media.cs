﻿using System;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace widget
{
    public class Media
    {
        readonly Pen _pen = new Pen(Color.White, 1);
        readonly Pen _penList = new Pen(Color.FromArgb(220, 220, 220), 1);
        readonly Font _font = new Font("Arial", 8);
        readonly SolidBrush _brush = new SolidBrush(Color.White);
        readonly SolidBrush _brushGree = new SolidBrush(Color.FromArgb(55, 230, 240));
        readonly SolidBrush _brushTran = new SolidBrush(Color.FromArgb(40, 255, 255, 255));

        // button
        readonly Point[] _prev = new Point[] { new Point(10, 41), new Point(20, 37), new Point(20, 45) };
        readonly Point[] _play = new Point[] { new Point(32, 34), new Point(40, 41), new Point(32, 48) };
        readonly Rectangle[] _paus = new Rectangle[] { new Rectangle(31, 37, 3, 10), new Rectangle(36, 37, 3, 10) };
        readonly Point[] _next = new Point[] { new Point(49, 37), new Point(59, 41), new Point(49, 45) };
        
        readonly Point[] _open = new Point[] { new Point(280, 56), new Point(289, 56), new Point(293, 60), new Point(293, 66), new Point(280, 66) };
        readonly Point[] _showList = new Point[] { new Point(305, 56), new Point(315, 56), new Point(310, 66) };
        readonly Point[] _hideList = new Point[] { new Point(305, 66), new Point(315, 66), new Point(310, 56) };
        readonly Point[] _volIcon = new Point[] { new Point(10, 59), new Point(14, 59), new Point(17, 56), new Point(17, 67), new Point(14, 64), new Point(10, 64) };

        readonly Point[] _btnCurrentPlay = new Point[] { new Point(5, 90), new Point(9, 93), new Point(5, 96) };
        
        Bitmap bmMedia;
        public string[] _list = new string[] { };
        public bool _isList = false;
        public bool _isPlay = false;
        public int _currentPlay = 0;
        public double _timePos;
        public Bitmap _bmFinish;
        public bool _isAuto = true;
        public bool _isMute = false;

        public void DrawMediaPlayer(Form1 frm)
        {
            Bitmap bm = new Bitmap(frm.ptb_media.Width, 300);
            Graphics g = Graphics.FromImage(bm);
            g.DrawRectangle(_pen, 0, 0, frm.ptb_media.Width - 1, 75);
            g.FillRectangle(_brushTran, 0, 0, frm.ptb_media.Width - 1, 75);

            // draw duration
            g.DrawLine(new Pen(Color.White, 3), 10, 25, 348, 25);

            // draw prev button
            g.FillPolygon(_brush, _prev);
            g.FillPolygon(_brush, _next);
            g.DrawPolygon(_pen, _open);
            // draw volume
            g.DrawPolygon(_pen, _volIcon);
            g.DrawLine(new Pen(Color.White, 2), 25, 62, 90, 62);

            bmMedia = bm;

            // update UI
            UpdateUiMedia(frm);
        }

        public void SetDuration(Form1 frm)
        {
            frm.tm_media.Stop();
            _timePos = (int)((frm._durX - 10) / (float)(338 / frm.mediaPlayer.currentMedia.duration));
            PlayMedia(frm);
        }

        public void IsMute(Form1 frm)
        {
            frm.mediaPlayer.settings.mute = _isMute = _isMute ? false : true;
            UpdateUiMedia(frm);
        }

        public void IsAuto(Form1 frm)
        {
            _isAuto = _isAuto ? false : true;
            UpdateUiMedia(frm);
        }

        public void UpdateUiMedia(Form1 frm)
        {
            Bitmap bm = new Bitmap(bmMedia);
            Graphics g = Graphics.FromImage(bm);
            if (_isPlay)
            {
                g.FillRectangles(_brush, _paus);
            }
            else
            {
                g.FillPolygon(_brush, _play);
            }

            SolidBrush _autoBrush = _isAuto ? _brushGree : _brush;
            g.DrawString("Auto", _font, _autoBrush, 325, 55);

            // Volume
            if (_isMute)
            {
                g.DrawLine(_pen, 9, 67, 18, 56);
            }
            g.DrawLine(new Pen(Color.FromArgb(0, 183, 195), 2), 25, 62, (float)(frm.mediaPlayer.settings.volume * 0.65 + 25), 62);
            g.DrawString(frm.mediaPlayer.settings.volume.ToString(), _font, _brush, 95, 55);

            // process list
            if (_isList)
            {
                int startIndex, endIndex;
                if (_currentPlay < 5)
                {
                    startIndex = 0;
                    endIndex = 12;
                }
                else if (_currentPlay >= 5 && _currentPlay <= _list.Length - 7)
                {
                    startIndex = _currentPlay - 5;
                    endIndex = _currentPlay + 7;
                }
                else
                {
                    startIndex = _list.Length - 12;
                    endIndex = _list.Length;
                }
                g = DrawPlayList(startIndex, endIndex, g);
            }
            else
            {
                g.FillPolygon(_brush, _showList);
            }

            frm.ptb_media.Image = bm;
            _bmFinish = bm;
        }

        private Graphics DrawPlayList(int startIndex, int endIndex, Graphics _graph)
        {
            int _itemY = 0;
            for (int i = startIndex; i < endIndex; i++)
            {
                string titleTmp = Path.GetFileName(_list[i]);
                SolidBrush _brusTmp = i == _currentPlay ? _brushGree : _brush;
                _graph.DrawString((i + 1).ToString() + ". " + titleTmp, _font, _brusTmp, 10, (_itemY * 17) + 86);
                _itemY++;
            }
            _graph.FillPolygon(_brushGree, _btnCurrentPlay);
            _graph.FillPolygon(_brush, _hideList);
            _graph.DrawRectangle(_penList, 0, 75, 357, 224);
            return _graph;
        }

        public void CurrentPlay()
        {
            int _curTmp = (_currentPlay >= 5) && (_currentPlay <= _list.Length - 7) ? 5 : _currentPlay;
            _curTmp = (_currentPlay > _list.Length - 7) ? (12 - _list.Length + _currentPlay) : _curTmp;
            _btnCurrentPlay[0].Y = 90 + (_curTmp * 17);
            _btnCurrentPlay[1].Y = 93 + (_curTmp * 17);
            _btnCurrentPlay[2].Y = 96 + (_curTmp * 17);
        }

        public void Browse(Form1 frm)
        {
            using (var fbd = new FolderBrowserDialog())
            {
                DialogResult result = fbd.ShowDialog();
                if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
                {
                    _list = Directory.GetFiles(fbd.SelectedPath);
                    _currentPlay = 0;
                    _timePos = 0;
                    _isList = true;
                    if (_isAuto)
                    {
                        PlayMedia(frm);
                    }
                    ShowList(frm);
                }
                else
                {
                    return;
                }
            }
        }

        public void ShowList(Form1 frm)
        {
            if (_list.Length != 0)
            {
                _isList = true;
                UpdateUiMedia(frm);
            }
            else
            {
                MessageBox.Show("Please browse to a play list!", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        public void HideList(Form1 frm)
        {
            _isList = false;
            UpdateUiMedia(frm);
        }

        public void PlayMedia(Form1 frm)
        {
            if (_list.Length != 0)
            {
                frm.mediaPlayer.URL = _list[_currentPlay];
                frm.mediaPlayer.Ctlcontrols.currentPosition = _timePos;
                frm.mediaPlayer.Ctlcontrols.play();
                _isPlay = true;
                CurrentPlay();
                UpdateUiMedia(frm);
                frm.tm_media.Start();
            }
            else
            {
                MessageBox.Show("Please browse to a play list!", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        public void PauseMedia(Form1 frm)
        {
            if (_list.Length != 0)
            {
                _timePos = frm.mediaPlayer.Ctlcontrols.currentPosition;
                frm.mediaPlayer.Ctlcontrols.pause();
                _isPlay = false;
                UpdateUiMedia(frm);
            }
            else
            {
                MessageBox.Show("Please browse to a play list!", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            frm.tm_media.Stop();
        }

        public void PrevMedia(Form1 frm)
        {
            if (_currentPlay != 0)
            {
                _timePos = 0;
                _currentPlay--;
                _isPlay = true;
                PlayMedia(frm);
            }
        }

        public void NextMedia(Form1 frm)
        {
            if (_currentPlay < _list.Length - 1)
            {
                _timePos = 0;
                _currentPlay++;
                _isPlay = true;
                PlayMedia(frm);
            }
        }

        public void SetVolume(Form1 frm)
        {
            int _vol = (int)((frm._volX - 25) / 0.65);
            frm.mediaPlayer.settings.volume = _vol;
            UpdateUiMedia(frm);
        }

        public void VolUp(Form1 frm)
        {
            if (frm.mediaPlayer.settings.volume <= 99)
            {
                frm.mediaPlayer.settings.volume += 1;
                UpdateUiMedia(frm);
            }
        }

        public void VolDown(Form1 frm)
        {
            if (frm.mediaPlayer.settings.volume >= 1)
            {
                frm.mediaPlayer.settings.volume -= 1;
                UpdateUiMedia(frm);
            }
        }
    }
}
