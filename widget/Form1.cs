﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Management;
using System.Windows.Forms;
using widget.plugin;

namespace widget
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        exit _exit = new exit();
        processCpu _cpu = new processCpu();
        clock _clock = new clock();
        calendar _calendar = new calendar();
        weather _weather = new weather();
        string _cpuName = "";
        Bitmap bmClock;

        // Media
        Media _med = new Media();
        public int _volX, _durX;

        private void Form1_Load(object sender, EventArgs e)
        {
            // draw exit button
            ptb_exit.Image = _exit.drawExitButton();
            ptb_exit.Cursor = Cursors.Hand;
            // draw clock
            bmClock = _clock.drawClock(this);
            _cpuName = _cpu.systemInfo();
            // draw calendar
            ptb_calendar.Image = _calendar.drawCalendar();
            // draw weather
            ptb_weather.Image = _weather.getDataWeather(this);

            this.BackColor = Color.FromArgb(50, 50, 50);
            Screen[] _screen = Screen.AllScreens;
            int _nScreen = _screen.Length;
            this.StartPosition = FormStartPosition.Manual;
            this.Location = new Point(Screen.PrimaryScreen.WorkingArea.Width - this.Width, 0);
            this.Height = Screen.PrimaryScreen.WorkingArea.Height;

            // media
            _med.DrawMediaPlayer(this);
            timer_processes.Start();
        }

        int _timeWeather = 0;

        private void timer_processes_Tick(object sender, EventArgs e)
        {
            // run clock
            ptb_clock.Image = _clock.runClock(bmClock, this);

            //-- Draw processes
            ptb_cpu.Image = _cpu.drawProcesses(_cpuName, this);

            // Draw weather
            _timeWeather++;
            if (_timeWeather == 1800)
            {
                _timeWeather = 0;
                ptb_weather.Image = Properties.Resources.bg_weather;
                ptb_weather.Image = _weather.getDataWeather(this);
            }

            // Dispos
            GC.Collect();
        }

        private void ptb_exit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void ptb_media_Click(object sender, EventArgs e)
        {
            var mouseEvent = e as MouseEventArgs;
            int _posX = mouseEvent.X;
            int _posY = mouseEvent.Y;
            // Set duration
            if (_posY >= 24 && _posY <= 29)
            {
                if (_posX >= 10 && _posX <= 348)
                {
                    _durX = _posX;
                    _med.SetDuration(this);
                }
            }
            // next, prev, pause, play
            else if (_posY >= 34 && _posY <= 48)
            {
                if (_posX >= 10 && _posX <= 20)
                {
                    _med.PrevMedia(this);
                }
                else if (_posX >= 32 && _posX <= 40)
                {
                    if (_med._isPlay)
                    {
                        _med._isPlay = false;
                        _med.PauseMedia(this);
                    }
                    else
                    {
                        _med._isPlay = true;
                        _med.PlayMedia(this);
                    }
                }
                else if (_posX >= 49 && _posX <= 59)
                {
                    _med.NextMedia(this);
                }
            }
            // volum & open and process list
            else if (_posY >= 56 && _posY <= 67)
            {
                if (_posX >= 10 && _posX <= 17)
                {
                    _med.IsMute(this);
                }
                else if (_posX >= 25 && _posX <= 90)
                {
                    _volX = _posX;
                    _med.SetVolume(this);
                }
                else if (_posX >= 280 && _posX <= 293)
                {
                    _med.Browse(this);
                }
                else if (_posX >= 305 && _posX <= 315)
                {
                    if (_med._isList)
                    {
                        _med._isList = false;
                        _med.HideList(this);
                    }
                    else
                    {
                        _med._isList = true;
                        _med.ShowList(this);
                    }
                }
                else if (_posX >= 325 && _posX <= 350)
                {
                    _med.IsAuto(this);
                }
            }
        }

        private void ptb_media_MouseMove(object sender, MouseEventArgs e)
        {
            var mouseEvent = e as MouseEventArgs;
            int _posX = mouseEvent.X;
            int _posY = mouseEvent.Y;
            // Set duration
            if (_posY >= 24 && _posY <= 29)
            {
                ptb_media.Cursor = (_posX >= 10 && _posX <= 348) ? Cursors.Hand : Cursors.Default;
            }
            // next, prev, pause, play
            else if (_posY >= 34 && _posY <= 48)
            {
                ptb_media.Cursor = ((_posX >= 10 && _posX <= 20) || (_posX >= 32 && _posX <= 40) || (_posX >= 49 && _posX <= 59)) ? Cursors.Hand : Cursors.Default;
            }
            else if (_posY >= 56 && _posY <= 67)
            {
                ptb_media.Cursor = ((_posX >= 10 && _posX <= 17) || (_posX >= 25 && _posX <= 90) || (_posX >= 280 && _posX <= 293) || (_posX >= 305 && _posX <= 315) || (_posX >= 325 && _posX <= 350)) ? Cursors.Hand : Cursors.Default;
            }
            else
            {
                ptb_media.Cursor = Cursors.Default;
            }
        }

        private void tm_media_Tick(object sender, EventArgs e)
        {
            Bitmap _bmMedia = new Bitmap(_med._bmFinish);
            Graphics g = Graphics.FromImage(_bmMedia);
            double _duration = mediaPlayer.currentMedia.duration;
            double _durationCur = mediaPlayer.Ctlcontrols.currentPosition;
            double _speed = 338 / _duration;
            // cal time
            string _start = ((int)(_durationCur / 60)).ToString().PadLeft(2, '0') + ":" + ((int)(_durationCur % 60)).ToString().PadLeft(2, '0');
            string _end = ((int)(_duration / 60)).ToString().PadLeft(2, '0') + ":" + ((int)(_duration % 60)).ToString().PadLeft(2, '0');
            float _posX = (float)((_durationCur * _speed) + 10);
            
            g.DrawString(_start, new Font("Arial", 8), new SolidBrush(Color.White), 8, 7);
            g.DrawString(_end, new Font("Arial", 8), new SolidBrush(Color.White), 320, 7);

            g.DrawLine(new Pen(Color.FromArgb(0, 183, 195), 3), 10, 25, _posX, 25);

            // is auto
            if ((int)_durationCur >= ((int)_duration - 1))
            {
                if (_med._isAuto)
                {
                    _med.NextMedia(this);
                }
                else
                {
                    _med._isPlay = false;
                    _med.UpdateUiMedia(this);
                }
            }

            // animate music
            Random rd = new Random();
            g.DrawLine(new Pen(Color.White, 1), new Point(122, 65), new Point(122, rd.Next(57, 65)));
            g.DrawLine(new Pen(Color.White, 1), new Point(124, 65), new Point(124, rd.Next(57, 65)));
            g.DrawLine(new Pen(Color.White, 1), new Point(126, 65), new Point(126, rd.Next(57, 65)));
            g.DrawLine(new Pen(Color.White, 1), new Point(128, 65), new Point(128, rd.Next(57, 65)));
            g.DrawLine(new Pen(Color.White, 1), new Point(130, 65), new Point(130, rd.Next(57, 65)));
            g.DrawLine(new Pen(Color.White, 1), new Point(132, 65), new Point(132, rd.Next(57, 65)));
            ptb_media.Image = _bmMedia;

            // display video
            // mediaPlayer.uiMode = "none";
            // mediaPlayer.Visible = true;

            GC.Collect();
        }

        private void ptb_media_DoubleClick(object sender, EventArgs e)
        {
            tm_media.Stop();
            var mouseEvent = e as MouseEventArgs;
            int _posX = mouseEvent.X;
            int _posY = mouseEvent.Y;
            if (_posX >= 10 && _posX <= 338)
            {
                if (_posY > 89)
                {
                    int _curPlay = (int)((_posY - 89) / 17);
                    if (_med._currentPlay >= 5 && _med._currentPlay <= _med._list.Length - 7)
                    {
                        _med._currentPlay += _curPlay > 5 ? Math.Abs(_curPlay - 5) : -(Math.Abs(_curPlay - 5));
                    }
                    else if (_med._currentPlay > _med._list.Length - 7)
                    {
                        int _curPlay2 = 12 - (_med._list.Length - _med._currentPlay);
                        _med._currentPlay += _curPlay > _curPlay2 ? Math.Abs(_curPlay - _curPlay2) : -(Math.Abs(_curPlay - _curPlay2));
                    }
                    else
                    {
                        _med._currentPlay = _curPlay;
                    }
                    _med.PlayMedia(this);
                }
            }
        }

        private void cbb_city_SelectedIndexChanged(object sender, EventArgs e)
        {
            ptb_media.Focus();
        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case (Keys.Up):
                    {
                        tm_media.Stop();
                        _med.PrevMedia(this);
                        break;
                    }
                case (Keys.Down):
                    {
                        tm_media.Stop();
                        _med.NextMedia(this);
                        break;
                    }
                case (Keys.Left):
                    {
                        _med.VolDown(this);
                        break;
                    }
                case (Keys.Right):
                    {
                        _med.VolUp(this);
                        break;
                    }
                case (Keys.Space):
                    {
                        if (_med._isPlay)
                            _med.PauseMedia(this);
                        else
                            _med.PlayMedia(this);
                        break;
                    }
            }
        }
    }
}
